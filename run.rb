#!/usr/bin/env ruby
# frozen_string_literal: true

require 'find'
require 'logger'
require 'net/http'
require 'json'
require 'set'
require 'csv'
require 'matrix'
require 'googleauth'

class PromptResult
  attr_reader :prompt, :response, :duration, :embedding_score

  def initialize(prompt:, response:, duration:, logger:)
    @prompt = prompt
    @response = response
    @duration = duration
    @logger = logger
  end

  def to_h
    {
      reference: reference,
      model_engine: model_engine,
      model_name: model_name,
      duration: duration
    }
  end

  def reference
    "path: #{prompt[:filename]}, cursor: #{prompt[:cursor_id]}"
  end

  def model_engine
    body&.dig("model", "engine")
  end

  def model_name
    body&.dig("model", "name")
  end

  def jaccard
    @jaccard ||= jaccard_similarity_ngrams(expected_response, real_response, 3)
  end

  def expected_response
    prompt[:content]
  end

  def real_response
    @real_response ||= parse_content
  end

  def set_embedding_score(score)
    @embedding_score = score
  end

  private

  attr_reader :logger

  def body
    @body ||= JSON.parse(response.body)
  end

  def parse_content
    data = body
    unless data['choices']&.first
      logger.error "choices are missing, body: #{response.body}"
      return ''
    end

    data['choices'].first['text']
  end

  def ngrams(string, size)
    string.chars.each_cons(size).map(&:join)
  end

  def jaccard_similarity_ngrams(string1, string2, ngram_size)
    ngrams1 = ngrams(string1, ngram_size).to_set
    ngrams2 = ngrams(string2, ngram_size).to_set

    intersection = ngrams1.intersection(ngrams2).count.to_f
    union = ngrams1.union(ngrams2).count.to_f
    return 0 if union == 0

    (intersection / union).round(3)
  end
end

class ResultsPresenter
  AI_ENABLEMENT_DEV_GCP_PROJECT_ID = 'ai-enablement-dev-69497ba7'
  # embeddings API requires content to calculate embeddings
  # any nil content would cause the API to error and you lose the entire batch
  NO_RESULT = 'N/A'

  def initialize(logger:)
    @data = []
    @logger = logger
  end

  def add(result)
    data << result

    logger.info "added to results: #{result.to_h}"
    logger.info <<~DIFF
      <<< expected response:
      #{result.expected_response}
      >>> real response:
      #{result.real_response}
    DIFF
  end

  def present
    fname = ENV['EXPORT_TO']

    if fname
      set_embedding_scores
      export_to_csv(fname)
    else
      logger.warn "EXPORT_TO env var not set, skipping export to CSV"
    end
  end

  private

  attr_reader :data, :logger

  def set_embedding_scores
    expected = data.map { |res| res.expected_response || NO_RESULT }
    real = data.map { |res| res.real_response || NO_RESULT }
    scores = similarity_scores(expected, real)
    data.each_with_index { |res, idx| res.set_embedding_score(scores[idx]) }
  end

  def export_to_csv(fname)
    File.delete(fname) if File.exist?(fname)

    CSV.open(fname, 'w+') do |csv|
      csv << ["snippet", "jaccard similarity", "duration", "model engine", "model name", "expected content", "real response", "similarity score"]

      data.each do |res|
        csv << [res.reference, res.jaccard, res.duration, res.model_engine, res.model_name, res.expected_response, res.real_response, res.embedding_score]
      end
    end

    logger.info "exported to #{fname}"
  end

  def gcloud_key
    @access_token ||= begin
      if ENV['GOOGLE_APPLICATION_CREDENTIALS']
        # Authenticate via a service account by setting GOOGLE_APPLICATION_CREDENTIALS=path/to/keyfile.json
        authorizer = Google::Auth::ServiceAccountCredentials.from_env(scope: 'https://www.googleapis.com/auth/cloud-platform')
        authorizer.fetch_access_token!["access_token"]
      else
        `gcloud auth print-access-token`
      end
    end
  end

  def similarity_scores(expected, actual)
    embeddings = get_text_embeddings(expected + actual)
    expected_embeddings = embeddings[0...expected.length]
    actual_embeddings = embeddings[expected.length..]

    expected_embeddings.map.with_index do |exp, i|
      next 0 if expected[i] == NO_RESULT || actual[i] == NO_RESULT
      exp.dot(actual_embeddings[i]) unless exp.nil? || actual_embeddings[i].nil?
    end
  end

  def get_text_embeddings(texts)
    gcp_project = ENV.fetch('GCP_PROJECT', AI_ENABLEMENT_DEV_GCP_PROJECT_ID)
    gcp_region = ENV.fetch('GCP_REGION', 'us-central1')
    embeddings_uri = URI("https://#{gcp_region}-aiplatform.googleapis.com/v1/projects/#{gcp_project}/locations/#{gcp_region}/publishers/google/models/textembedding-gecko:predict")

    embeddings = []
    texts.each_slice(5) do |text_slice|
      data = {
        instances: text_slice.map { |t| { content: t } }
      }

      response = Net::HTTP.post embeddings_uri, data.to_json, {
        'Content-Type': 'application/json',
        'Authorization' => "Bearer #{gcloud_key}"
      }

      begin
        result = JSON.parse(response.body)&.dig('predictions')&.map do |emb|
          Vector.elements(emb.dig('embeddings', 'values'))
        end
      rescue StandardError => e
        logger.error e
        logger.error response.body
        logger.error text_slice
      end

      embeddings += result
    end

    embeddings
  end
end

class PromptRunner
  # examples:
  # <cursor-1>expected output</cursor-1>
  # <cursor-1-generation>expected output</cursor-1> - 'generation' is used as intent parameter
  CURSOR_REGEXP = %r{<cursor-(?<num>\d+)-?(?<intent>[a-z]*)?>(?<content>.*?)</cursor-\d+>}m
  DELAY = 0.5

  def initialize(snippet_location:)
    @snippet_location = snippet_location
    @logger = Logger.new(STDOUT)
    @logger.level = ENV['DEBUG'] == '1' ? Logger::DEBUG : Logger::INFO
  end

  def run
    presenter = ResultsPresenter.new(logger: logger)

    prompt_files.each do |filename|
      prompts(filename).each do |prompt|
        response, duration = *request(prompt)

        unless ['200', '201'].include?(response.code)
          logger.error "response code is #{response.code}, body: #{response.body}"
          next
        end

        prompt_res = PromptResult.new(prompt: prompt, response: response, duration: duration, logger: logger)
        presenter.add(prompt_res)
        sleep(DELAY)
      rescue StandardError => ex
        logger.error "#{prompt[:filename]} #{prompt[:cursor_id]}: #{ex.message}\n#{ex.backtrace}"
      end
    end

    presenter.present
  end

  private

  attr_reader :snippet_location, :logger, :export_csv

  def rails_url
    # e.g. http://127.0.0.1:3000/api/v4/code_suggestions/completions
    raise "RAILS_URL env variable is not set" unless ENV['RAILS_URL']

    ENV['RAILS_URL']
  end

  def rails_token
    raise "RAILS_TOKEN env variable is not set" unless ENV['RAILS_TOKEN']

    ENV['RAILS_TOKEN']
  end

  def prompt_files
    files = []

    Find.find(snippet_location) do |path|
      next unless File.file?(path)
      next if File.basename(path).start_with?('.')

      files << path
    end

    files
  end

  def prompts(filename)
    matches = []
    File.read(filename).scan(CURSOR_REGEXP) { matches << Regexp.last_match }

    matches.map do |match|
      {
        filename: filename,
        cursor_id: match[:num],
        content: match[:content],
        intent: match[:intent],
        above: match.pre_match.gsub(CURSOR_REGEXP, '\k<content>'),
        below: match.post_match.gsub(CURSOR_REGEXP, '\k<content>')
      }
    end
  end

  def request(prompt)
    logger.info "path: #{prompt[:filename]}, cursor: #{prompt[:cursor_id]}"

    headers = {
      "PRIVATE-TOKEN" => rails_token,
      "Content-Type" => "application/json"
    }

    data = {
      current_file: {
        file_name: prompt[:filename],
        content_above_cursor: prompt[:above],
        content_below_cursor: prompt[:below]
      }
    }
    data[:intent] = prompt[:intent] unless prompt[:intent].empty?

    t0 = Time.now.to_f
    logger.debug "request #{[rails_url, data, headers]}"
    res = Net::HTTP.post URI(rails_url), data.to_json, headers
    logger.debug "response #{res.to_hash}"

    duration = Time.now.to_f - t0
    logger.info "duration: #{duration}"

    [res, duration]
  end
end

location = ARGV[0] || 'snippets'
PromptRunner.new(snippet_location: location).run
