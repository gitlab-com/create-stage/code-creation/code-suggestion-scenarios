#!/usr/bin/env ruby
# frozen_string_literal: true

activation_code = ENV['GITLAB_ACTIVATION_CODE']
raise ArgumentError, "Please provide a Cloud License activation code as GITLAB_ACTIVATION_CODE" unless activation_code

result = ::GitlabSubscriptions::ActivateService.new.execute(activation_code)
if result[:success]
  puts 'Activation successful'
else
  puts 'Activation unsuccessful'
  puts Array(result[:errors]).join(' ')
  exit 1
end

ApplicationSetting.current.update(instance_level_code_suggestions_enabled: true)

# Override the prompt to change the password upon login
user = User.find(1)
user.update!(password_expires_at: nil)
