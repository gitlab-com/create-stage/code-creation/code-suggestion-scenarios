# CI Automation

Evaluation of the Code Suggestions snippets can be performed in merge requests, or you can [start a new pipeline](https://gitlab.com/gitlab-com/create-stage/code-creation/code-suggestion-scenarios/-/pipelines/new). In either case, after you trigger the manual `run_scenarios` job it does the following:

1. Launches a GDK instance in a Docker container.
2. Executes `run.rb` to get code suggestions via that GDK instance.
3. Saves the results as an artifact named `scores.csv`.

## How it works

### Pull, configure, and launch GDK

The automation uses a Dockerized GDK instance that was created to [run GitLab end-to-end tests](https://docs.gitlab.com/ee/development/testing_guide/end_to_end/test_pipelines.html#e2etest-on-gdk).
By default the `run_scenarios` job uses the latest GDK Docker image created in `gitlab-org/gitlab` master pipelines, `registry.gitlab.com/gitlab-org/gitlab/gitlab-qa-gdk:master`.
A different image build can be used by setting `GITLAB_SHA` to the SHA of a `gitlab-org/gitlab` commit that has a pipeline that
includes the `build-gdk-image` job (which includes most GitLab commits that have application code changes).

When the GDK Docker container starts, the `run_scenarios` job activates a GitLab cloud license via an activation code (set via the `GITLAB_ACTIVATION_CODE` CI variable).
If you override the activation code it must be a valid code created by a CustomersDot instance and the license must include a Code Suggestions add-on purchase.
The GDK instance is configured (via `CUSTOMER_PORTAL_URL`) to use the staging CustomersDot instance, https://customers.staging.gitlab.com.

The automation also enables Code Suggestions on the GDK instance. It is configured to use https://codesuggestions.staging.gitlab.com
via `CODE_SUGGESTIONS_BASE_URL`.

### Run scenarios

When the GDK instance is ready to respond to requests, the automation executes the `run.rb` script to collect suggestions and analyze them.
Text embeddings are used to calculate similarity scores, and those [text embeddings are fetched from the Vertex AI API](https://cloud.google.com/vertex-ai/docs/generative-ai/embeddings/get-text-embeddings).
By default the CI automation uses the Vertex AI API via the GCP project `gitlab-qa-ai-tests-2924ef`, which can be overridden via `GCP_PROJECT`.
Access to the API uses a service account, with the credentials provided by a key file in JSON format and stored as a CI file variable `GOOGLE_APPLICATION_CREDENTIALS`.

The results are saved as an artifact named `scores.csv`.
