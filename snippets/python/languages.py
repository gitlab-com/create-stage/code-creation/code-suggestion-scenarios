languages = {
    'en': 'English',
    'hello': 'Hello',
    'goodbye': 'Goodbye',
    'bye': 'Goodbye',

    'fr': 'Français',
    'hello_fr': 'Bonjour',
    'goodbye_fr': 'Au revoir',
    'bye_fr': 'Au revoir',

}

def translate(word):
    <cursor-1>if word in languages:
        return languages[word]
    else:
        return word</cursor-1>
