function createRandomObject(): any {
    // generate a random obj
    const randomObj = {};

    for (let i = 0; i < 10; i++) {
    const randomKey = Math.random().toString();
    randomObj[randomKey] = Math.random();
    }
    return randomObj;
}

function calculateObjectDepth(obj: any)<cursor-1>: number {
    let depth = 0;

    function recurse(obj: any) {
      depth++;
      Object.values(obj).forEach(value => {
        if(typeof value === 'object') {
          recurse(value);
        }
      });
      return depth;
    }

    return recurse(obj);</cursor-1>
}

function printDebug() {
    <cursor-2>
    const randomObj = createRandomObject();
    const depth = calculateObjectDepth(randomObj);
    console.log('Depth:', depth);
    </cursor-2>
}
