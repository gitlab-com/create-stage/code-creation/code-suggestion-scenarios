# frozen_string_literal: true

require 'carrierwave/orm/activerecord'

class Project < ApplicationRecord
  has_many :project_topics, -> { order(:id) }, class_name: 'Projects::ProjectTopic'
  has_many :topics, through: :project_topics, class_name: 'Projects::Topic'

  attr_accessor :old_path_with_namespace
  attr_accessor :template_name
  attr_writer :pipeline_status
  attr_accessor :skip_disk_validation
  attr_writer :topic_list

  alias_attribute :title, :name

  # Relations
  belongs_to :pool_repository

  has_one :last_event, -> { order 'events.created_at DESC' }, class_name: 'Event'
  has_many :boards

  # Used by Projects::CleanupService to hold a map of rewritten object IDs
  mount_uploader :bfg_object_map, AttachmentUploader

  def self.with_api_entity_associations
    preload(:project_feature, :route, :topics, :group, :timelogs, namespace: [:route, :owner])
  end

  def self.for_topics(topics)
    where(<cursor-1>topics: topics)</cursor-1>
  end

  class << self
    # class method which searches for a project by its name<cursor-2>
    def self.find_by_name(name)
      find_by(name: name)
    end</cursor-2>
  end
end
