class UsernameAndEmailGenerator
  def initialize(prefix)
    @prefix = prefix
  end

  def generate
    <cursor-1>
    username = @prefix + rand(1000).to_s
    email = username + "@example.com"
    return [username, email]
    </cursor-1>
  end
end
