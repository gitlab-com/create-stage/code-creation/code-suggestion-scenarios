package db

import (
	"context"

	"github.com/ck3g/SomeDaysOfGo/hotel-reservation/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *MongoHotelStore) GetHotels(ctx context.Context, filter bson.M) ([]*types.Hotel, error) {
	var hotels []*types.Hotel
	resp, err := s.coll.Find(ctx, filter)
	if err != nil {
		return nil, err
	}
	if err := <cursor-1>resp.All(ctx, &hotels); err != nil {
		return nil, err
	}

	return hotels, nil
}</cursor-1>
