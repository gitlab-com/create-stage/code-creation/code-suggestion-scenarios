import re

"""regex pattern for an email"""
email = re.compile<cursor-1>(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")</cursor-1>

"""regex pattern for a phone number"""
phone = re.compile<cursor-2>(r"(^(\+\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$)")</cursor-2>
