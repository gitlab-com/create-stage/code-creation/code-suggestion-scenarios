# a NumericCalculator class that perform multiplication of 2 matrix<cursor-1> 
import numpy as np

class NumericCalculator:

  def __init__(self):
    pass

  def multiply_matrices(self, matrix1, matrix2):
    return np.matmul(matrix1, matrix2) 
</cursor-1>