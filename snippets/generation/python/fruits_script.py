# script that reads all records from fruits table that starts with an A<cursor-1>
from pg import DB

db = DB(dbname='testdb', host='pgserver', port=5432,
    user='scott', passwd='tiger')

db.query('select * from fruits where name like \'A%\'')    
</cursor-1>