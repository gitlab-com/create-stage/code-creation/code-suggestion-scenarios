# MarkdownReader class that reads content of a file appointed with file name and returns html of rendered markdown<cursor-1>
import markdown

class MarkdownReader:
    def __init__(self, filename):
        self.filename = filename
    
    def read(self):
        with open(self.filename, "r", encoding="utf-8") as input_file:
            text = input_file.read()
        return markdown.markdown(text)
</cursor-1>