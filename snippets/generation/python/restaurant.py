# Define models for a restaurant application using SQLAlchemy
# Define model for Restaurant with name, address, phone, review rating
<cursor-1>
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Restaurant(Base):
    __tablename__ = 'restaurants'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    phone = Column(String)
    rating = Column(Float)
</cursor-1>
# Define model for MenuItem with name, description, price, course
<cursor-2>
class MenuItem(db.Model):
    __tablename__ = 'menu_items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.Text)
    price = db.Column(db.Float, nullable=False)
    course = db.Column(db.String(20))
</cursor-2>
