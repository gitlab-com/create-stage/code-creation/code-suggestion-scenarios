// saveConfig functions that takes in string and saves it into temp file<cursor-1>
import * as temp from 'temp';

export async function saveConfig(config: string){
    const tmpFilePath = temp.path({ suffix: 'configs' });
    await fs.writeFile(tmpFilePath, config);
    await fs.unlink(tmpFilePath);
}
</cursor-1>