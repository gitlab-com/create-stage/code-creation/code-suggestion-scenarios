// Create a function to validate a UK postalcode
<cursor-1>
function validateUKPostcode(postcode: string): boolean {
    const postcodeRegex = /^[A-Z]{1,2}[0-9][0-9A-Z]?\s?[0-9][A-Z]{2}$/i;
    return postcodeRegex.test(postcode);
}
</cursor-1>
// Function to validate a US phone number
<cursor-2>
function validatePhoneNumber(phoneNumber: string): boolean {
    const regex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return regex.test(phoneNumber);
}
</cursor-2>
// Convert miles to kilometers and format in '123 km'
<cursor-3>
function milesToKm(miles: number): string {
    const km = miles * 1.60934;
    return `${Math.round(km)} km`;
}
</cursor-3>
