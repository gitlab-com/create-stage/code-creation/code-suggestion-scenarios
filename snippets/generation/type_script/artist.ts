// An Artist class that has popularity method that based on artist name display its popularity on Spotify<cursor-1>
import { SpotifyApi } from '@spotify/web-api-ts-sdk';

class Artist {
    constructor(
        public name: string, 
    ) {}

    public async popularity() {
        const sdk = SpotifyApi.withUserAuthorization("client-id", "https://localhost:3000", ["scope1", "scope2"]);
        const items = await sdk.search(this.name, ["artist"]);
        return items[0].popularity
    }
}
</cursor-1>