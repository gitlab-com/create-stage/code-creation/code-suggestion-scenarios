function isOdd(<cursor-1>num: number): boolean {
    return num % 2 !== 0;
  }
</cursor-1>