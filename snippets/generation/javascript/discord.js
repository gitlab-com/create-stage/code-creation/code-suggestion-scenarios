// Create a discord.js reminder bot using slash commands, this should allow users to set reminder for individual tasks.
<cursor-1>
import { Client, GatewayIntentBits } from 'discord.js';
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

// Create the slash command
const { SlashCommandBuilder } = require('@discordjs/builders');
const reminderCommand = new SlashCommandBuilder()
	.setName('reminder')
	.setDescription('Set a reminder')
	.addStringOption(option =>
		option.setName('text')
			.setDescription('The text for the reminder')
			.setRequired(true))
	.addIntegerOption(option =>
		option.setName('time')
			.setDescription('The time in minutes until the reminder')
			.setRequired(true));

// When the slash command is used
client.on('interactionCreate', async interaction => {
  if (!interaction.isCommand()) return;

  if (interaction.commandName === 'reminder') {
    const reminderText = interaction.options.getString('text');
    const minutesUntilReminder = interaction.options.getInteger('time');

    // Schedule the reminder
    setTimeout(() => {
      interaction.followUp(`Here is your reminder: ${reminderText}`);
    }, minutesUntilReminder * 60 * 1000);

    await interaction.reply(`I will remind you about ${reminderText} in ${minutesUntilReminder} minutes.`);
  }
});
</cursor-1>
