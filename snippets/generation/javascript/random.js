// Returns a random number(float) between min (inclusive) and max (exclusive)<cursor-1>
function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}
</cursor-1>
