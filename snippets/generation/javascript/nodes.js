function removeAllChildNodes(parent) {
    <cursor-1>while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }</cursor-1>
}

const container = document.querySelector('#container');
removeAllChildNodes(container);
