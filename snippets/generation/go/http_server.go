package main

import (
	"fmt"
	"net/http"
)

// create an HTTP server listening on httpPort
<cursor-1>
const httpPort = ":8080"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World"))
	})

	fmt.Println("Starting server on port", httpPort)
	err := http.ListenAndServe(httpPort, nil)
	if err != nil {
		panic(err)
	}
}
</cursor-1>
// Generate an HTTP server with a handler that returns "Hello World"
<cursor-2>
func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World"))
}

http.HandleFunc("/", handler)
</cursor-2>
// Structure Human with fields: firstName, lastName and age
<cursor-3>
type Human struct {
	firstName string
	lastName string
	age int
  }
</cursor-3>
// GenUniqueID is a function that returns a unique ID
<cursor-4>
func GenUniqueID() string {
	// generate and return a unique ID
  }
</cursor-4>
// create an HTTP handler that returns a color
// use the colorHandler
<cursor-5>
func colorHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Blue"))
  }

  http.HandleFunc("/", colorHandler)
</cursor-5>

