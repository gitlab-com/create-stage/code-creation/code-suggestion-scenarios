# an api for award_emoji
# that exposes name, user, url fields<cursor-1>
class AwardEmojiEntity < Grape::Entity
    expose :name
    expose :user, using: API::Entities::UserSafe
    expose :url
end
</cursor-1>  