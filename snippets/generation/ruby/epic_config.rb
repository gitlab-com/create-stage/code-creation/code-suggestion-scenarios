# frozen_string_literal: true

module Elastic
    module Latest
      # A module for epic config that defines elastic module with dynamic strict mappings which index id, iid, group_id all of integer type<cursor-1> 
      module EpicConfig
        extend Elasticsearch::Model::Indexing::ClassMethods
        extend Elasticsearch::Model::Naming::ClassMethods
        
        self.index_name = [Rails.application.class.module_parent_name.downcase, Rails.env, 'epics'].join('-')

        settings Elastic::Latest::Config.settings.to_hash.deep_merge(
          index: Elastic::Latest::Config.separate_index_specific_settings(index_name)
        )
  
        mappings dynamic: 'strict' do
          indexes :id, type: :integer
          indexes :iid, type: :integer
          indexes :group_id, type: :integer
        end
      end
    end
  end
  </cursor-1>