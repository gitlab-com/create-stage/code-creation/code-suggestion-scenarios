require 'carrierwave/orm/activerecord'

class Project < ApplicationRecord
  include Gitlab::ConfigHelper
  include Gitlab::VisibilityLevel

  def print_name
    puts name
  end

  # method which checks if name is valid<cursor-1>
  def valid_name?
    name.present? && name.length <= 255
  end</cursor-1>
